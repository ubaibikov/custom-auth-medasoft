package main

import (
	"flag"
	"go-work/custom-api/internal/app"
	"go-work/custom-api/internal/app/apiserver"
	"log"

	"github.com/BurntSushi/toml"
	_ "github.com/lib/pq"
)

var (
	configPath string
)

func init() {
	flag.StringVar(&configPath, "config-path", "../../configs/apiserver.toml", "path to config file")
}

func main() {

	flag.Parse()
	config := app.NewConfig()

	_, err := toml.DecodeFile(configPath, config)
	if err != nil {
		log.Fatal(err.Error())
	}

	s := apiserver.New(config)

	if err := s.Start(); err != nil {
		log.Fatal(err)
	}
}
