package apiserver

import (
	"go-work/custom-api/internal/app"
	"go-work/custom-api/internal/app/model"
	"go-work/custom-api/store"

	"github.com/gin-gonic/gin"
)

// Server ...
type Server struct {
	Config *app.Config
	store  *store.Store
}

// New ...
func New(config *app.Config) *Server {
	return &Server{
		Config: config,
	}
}

// Start ...
func (s *Server) Start() error {

	router := gin.Default()

	if err := s.configureStore(); err != nil {
		panic(err)
	}
	s.configureRouters(router)
	return router.Run(s.Config.Port)
}

func (s *Server) configureStore() error {
	st := store.New(s.Config)
	if err := st.Open(); err != nil {
		return err
	}

	s.store = st
	return nil
}

func (s *Server) configureRouters(routes *gin.Engine) {

	v1 := routes.Group("v1")
	{
		v1.POST("/register", s.Register)
		v1.POST("/login", s.Login)
		v1.GET("/users", s.AuthMiddleware, s.Index)
	}

}

// AuthMiddleware ...
func (s *Server) AuthMiddleware(c *gin.Context) {
	_, err := c.Cookie("auth_key")

	if err != nil {
		c.JSON(401, gin.H{
			"message": "Вы не авторизован",
		})
	} else {
		c.Next()
	}
}

// Index ...
func (s *Server) Index(c *gin.Context) {
	users, err := s.store.User().GetAll()
	if err != nil {
		panic(err)
	}
	c.JSON(200, gin.H{
		"message": "пользователи",
		"users":   users,
	})
}

// Register ..
func (s *Server) Register(c *gin.Context) {
	email := c.PostForm("email")
	password := c.PostForm("password")
	count, err := s.store.User().FindByEmail(email)
	if err != nil {
		panic(err)
	}

	if count > 0 {
		c.JSON(401, gin.H{
			"message": "Пользователь с таким мылом уже существует",
		})
	} else {
		_, err := s.store.User().Create(&model.User{Email: email, Password: password})
		if err != nil {
			panic(err)
		}
		var generetedToken string = GenerateToken()
		//  set user token
		c.SetCookie(
			"auth_key",
			generetedToken,
			3600,
			"/",
			"127.0.0.1",
			false,
			false,
		)

		c.JSON(200, gin.H{
			"auth_key": generetedToken,
		})
	}
}

// Login ...
func (s *Server) Login(c *gin.Context) {

	email := c.PostForm("email")
	password := c.PostForm("password")

	count, err := s.store.User().FindUser(email, password)
	if err != nil {
		panic(err)
	}

	if count == 0 {
		c.JSON(401, gin.H{
			"message": "Такого юсера и в помине нет.",
		})
	} else {
		var generetedToken string = GenerateToken()
		//  set user token
		c.SetCookie(
			"auth_key",
			generetedToken,
			3600,
			"/",
			"127.0.0.1",
			false,
			false,
		)

		c.JSON(200, gin.H{
			"auth_key": generetedToken,
		})
	}
}
