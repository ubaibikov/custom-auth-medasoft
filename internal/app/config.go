package app

// Config ...
type Config struct {
	Port        string `toml:"port"`
	DB_User     string `toml:"db_user"`
	DB_Host     string `toml:"db_host"`
	DB_Password string `toml:"db_password"`
	DB_Name     string `toml:"db_name"`
	DB_SSlmode  string `toml:"db_sslmode"`
	DB_PORT     string `toml:"db_port"`
}

var (
	configPath string
)

// NewConfig ...
func NewConfig() *Config {
	return &Config{}
}